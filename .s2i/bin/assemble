#!/bin/bash

set -x
set -eo pipefail

# During this initial build phase we are going to construct a new custom
# image based off the default PHP S2I builder. The resulting image can be
# run directly to create a standalone instance of WordPress not linked to
# anything, or it can be run as a S2I builder so that plugins, themes and
# language files can be pulled down from a repository and incorporated into
# the WordPress installation.

mkdir -p /opt/app-root/downloads

#----------------- EO 03.04.2019 --------------
# Install WP-CLI command line tool
# Will allow to create users: 
#   wp user create admin email@cern.ch --role=administrator
#     will create admin user with random password
# Must run from the WP installation directory (/opt/app-root/src)
# https://make.wordpress.org/cli/handbook/installing/ 
curl -Ls https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar > /opt/app-root/downloads/wp-cli.phar
# Verify it's ok
php /opt/app-root/downloads/wp-cli.phar --info
# Install
chmod +x /opt/app-root/downloads/wp-cli.phar
mkdir -p /opt/app-root/bin
mv /opt/app-root/downloads/wp-cli.phar /opt/app-root/bin/wp

# Move CERN Plugin to the plugin repository in downloads location
# Will need to be enabled using wp cli
#   wp plugin activate cern-sso-authentication
mv /tmp/src/plugins /opt/app-root/downloads

# Install mysql client to allow export import and otther mysql commands via WP CLI
# Temporary bricolaged
chmod a+x /tmp/src/bin/*
mv /tmp/src/bin/* /opt/app-root/bin

#----------------- /EO 03.04.2019 --------------


# This S2I assemble script is only used when creating the custom image.
# For when running the image, or using it as a S2I builder, we use a second
# set of custom S2I scripts. We now need to move these into the correct
# location and have the custom image use those by dropping in an image
# metadata file which overrides the labels of the base image.

mkdir -p /opt/app-root/s2i

mv /tmp/src/builder/assemble /opt/app-root/s2i/assemble
mv /tmp/src/builder/run /opt/app-root/s2i/run
mv /tmp/src/builder/save-artifacts /opt/app-root/s2i/save-artifacts

mkdir -p /tmp/.s2i

mv /tmp/src/builder/image_metadata.json /tmp/.s2i/image_metadata.json

rm -rf /tmp/src

# Fixup permissions on directories and files.

fix-permissions /opt/app-root

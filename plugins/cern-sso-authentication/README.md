# CERN SSO Wordpress Plugin

Autoupdate system based on 
- [https://github.com/YahnisElsts/plugin-update-checker](https://github.com/YahnisElsts/plugin-update-checker)

- How to release an update:
    - Update Version header in main file cern-sso-authentication.php
    - Update the 'stable-release' branch in gitlab.
    
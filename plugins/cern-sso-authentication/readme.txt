=== CERN SSO Authentication Plugin ===
Contributors: Emmanuel Ormancey
Tags: SSO, CERN
Requires at least: 4.6
Tested up to: 5.2
Stable tag: 4.3
Requires PHP: 5.2.4
License: GPLv3
License URI: https://www.gnu.org/licenses/gpl-3.0.html

CERN SSO Authentication plugin.

== Description ==

Get CERN SSO authentication ifnromation from the header, and maps the email field to an 
existing Wordpress account with the same mail field.
Allows to enter the admin/moderator part, if the user is known in the Wordpress internal database.

Requires an Openshift CERN-SSO-Proxy POD configured with forwarded headers.

== Installation ==

Install and enable plugin.

== Changelog ==

= 1.0 =
* Initial version.

== Upgrade Notice ==

